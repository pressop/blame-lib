<?php

/*
 * This file is part of the Blog package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Blame;

use Pressop\Component\Blame\Model\BlameInterface;

/**
 * Interface BlamerInterface
 *
 * @author Benjamin Georgeault
 */
interface BlamerInterface
{
    /**
     * @param BlameInterface $blameObject
     */
    public function generateCreateBlame(BlameInterface $blameObject);

    /**
     * @param BlameInterface $blameObject
     */
    public function generateUpdateBlame(BlameInterface $blameObject);
}
