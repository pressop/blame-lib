<?php

/*
 * This file is part of the Blog package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Blame\Bridge\Symfony;

use Pressop\Component\Blame\BlamerInterface;
use Pressop\Component\Blame\Model\BlameInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class Blamer
 *
 * @author Benjamin Georgeault
 */
class Blamer implements BlamerInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var string
     */
    private $userEntityClass;

    /**
     * Blamer constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param string $userEntityClass
     */
    public function __construct(TokenStorageInterface $tokenStorage, string $userEntityClass)
    {
        $this->tokenStorage = $tokenStorage;
        $this->userEntityClass = $userEntityClass;
    }

    /**
     * @inheritDoc
     */
    public function generateCreateBlame(BlameInterface $blameObject)
    {
        if (null !== $user = $this->getUser()) {
            $blameObject->setCreatedBy($user);
        }
    }

    /**
     * @inheritDoc
     */
    public function generateUpdateBlame(BlameInterface $blameObject)
    {
        if (null !== $user = $this->getUser()) {
            $blameObject->setUpdatedBy($user);
        }
    }

    /**
     * @return object|null
     */
    private function getUser()
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return null;
        }

        if (null === $user = $token->getUser()) {
            return null;
        }

        if ($user instanceof $this->userEntityClass) {
            return $user;
        }

        return null;
    }
}
