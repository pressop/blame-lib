<?php

/*
 * This file is part of the Blog package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Blame\Model;

/**
 * Interface BlameInterface
 *
 * @author Benjamin Georgeault
 */
interface BlameInterface
{
    /**
     * @return object|null
     */
    public function getCreatedBy();

    /**
     * @param object $createdBy
     * @return $this
     */
    public function setCreatedBy($createdBy);

    /**
     * @return object|null
     */
    public function getUpdatedBy();

    /**
     * @param object $updatedBy
     * @return $this
     */
    public function setUpdatedBy($updatedBy);
}
