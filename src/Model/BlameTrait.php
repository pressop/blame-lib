<?php

/*
 * This file is part of the Blog package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Blame\Model;

/**
 * Trait BlameTrait
 *
 * @author Benjamin Georgeault
 * @see BlameInterface
 */
trait BlameTrait // implements BlameInterface
{
    /**
     * @var object|null
     */
    protected $createdBy;

    /**
     * @var object|null
     */
    protected $updatedBy;

    /**
     * @return object|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param object $createdBy
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return object|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param object $updatedBy
     * @return $this
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }
}
