<?php

/*
 * This file is part of the Blog package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Blame\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Pressop\Component\Blame\BlamerInterface;
use Pressop\Component\Blame\Model\BlameInterface;

/**
 * Class BlameSubscriber
 *
 * @author Benjamin Georgeault
 */
class BlameSubscriber implements EventSubscriber
{
    /**
     * @var BlamerInterface
     */
    private $blamer;

    /**
     * BlameSubscriber constructor.
     * @param BlamerInterface $blamer
     */
    public function __construct(BlamerInterface $blamer)
    {
        $this->blamer = $blamer;
    }

    /**
     * @inheritDoc
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function prePersist(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        if ($entity instanceof BlameInterface) {
            $uow = $event->getEntityManager()->getUnitOfWork();
            $oldUser = $entity->getCreatedBy();
            $this->blamer->generateCreateBlame($entity);

            if ($oldUser !== $user = $entity->getCreatedBy()) {
                $uow->propertyChanged($entity, 'createdBy', $oldUser, $user);
                $uow->scheduleExtraUpdate($entity, [
                    'createdBy' => [$oldUser, $user],
                ]);
            }
        }
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function preUpdate(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        if ($entity instanceof BlameInterface) {
            $uow = $event->getEntityManager()->getUnitOfWork();
            $oldUser = $entity->getUpdatedBy();
            $this->blamer->generateUpdateBlame($entity);

            if ($oldUser !== $user = $entity->getCreatedBy()) {
                $uow->propertyChanged($entity, 'updatedBy', $oldUser, $user);
                $uow->scheduleExtraUpdate($entity, [
                    'updatedBy' => [$oldUser, $user],
                ]);
            }
        }
    }
}
