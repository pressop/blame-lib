<?php

/*
 * This file is part of the Blog package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Blame\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Pressop\Component\Blame\Model\BlameInterface;

/**
 * Class MappingSubscriber
 *
 * @author Benjamin Georgeault
 */
class MappingSubscriber implements EventSubscriber
{
    /**
     * @var string
     */
    private $userEntityClass;

    /**
     * @var string[]
     */
    private $namespaces;

    /**
     * MappingSubscriber constructor.
     * @param string $userEntityClass
     * @param string[] $namespaces Use to apply this mapping only for some entity namespaces.
     */
    public function __construct(string $userEntityClass, array $namespaces = [])
    {
        $this->userEntityClass = $userEntityClass;
        $this->namespaces = $namespaces;
    }

    /**
     * @inheritDoc
     */
    public function getSubscribedEvents()
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();

        if (false === $mappingAllowed = empty($this->namespaces)) {
            foreach ($this->namespaces as $namespace) {
                if (strstr($reflectionClass->getName(), $namespace)) {
                    $mappingAllowed = true;
                    break;
                }
            }
        }

        if (true === $mappingAllowed && $reflectionClass->implementsInterface(BlameInterface::class)) {
            $this->mappingBlame($metadata);
        }
    }

    /**
     * @param ClassMetadata $metadata
     */
    public function mappingBlame(ClassMetadata $metadata)
    {
        if (!$metadata->hasAssociation('createdBy')) {
            $metadata->mapManyToOne([
                'fieldName' => 'createdBy',
                'targetEntity' => $this->userEntityClass,
                'joinColumns' => [[
                    'onDelete' => 'SET NULL',
                ]],
                'fetch' => ClassMetadataInfo::FETCH_EAGER,
            ]);
        }

        if (!$metadata->hasAssociation('updatedBy')) {
            $metadata->mapManyToOne([
                'fieldName' => 'updatedBy',
                'targetEntity' => $this->userEntityClass,
                'joinColumns' => [[
                    'onDelete' => 'SET NULL',
                ]],
                'fetch' => ClassMetadataInfo::FETCH_EAGER,
            ]);
        }
    }
}
